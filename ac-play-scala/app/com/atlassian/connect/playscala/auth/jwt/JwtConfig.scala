package com.atlassian.connect.playscala.auth.jwt

import com.atlassian.connect.playscala.AcConfigured
import com.atlassian.jwt.core.http.auth.JwtAuthenticator
import com.atlassian.jwt.core.writer.NimbusJwtWriterFactory
import com.atlassian.jwt.core.reader.NimbusJwtReaderFactory
import com.atlassian.connect.playscala.store.DbConfiguration
import play.api.mvc.RequestHeader
import play.mvc.Http.Response
import play.api.Play.current

trait JwtConfig extends DbConfiguration with AcConfigured {

  val JwtExpirySecondsProperty: String = "com.atlassian.connect.jwt.expiry_seconds"

  /**
   * Default of 3 minutes.
   */
  private val JwtExpirySecondsDefault: Int = 60 * 3
  private lazy val jwtExpiryWindowSeconds: Int = current.configuration.getInt(JwtExpirySecondsProperty).getOrElse(JwtExpirySecondsDefault)

  implicit lazy val jwtAuthorizationGenerator = new JwtAuthorizationGenerator(new NimbusJwtWriterFactory(), jwtExpiryWindowSeconds)(acConfig)
  private lazy val jwtIssuerService: ACPlayJwtIssuerService = new ACPlayJwtIssuerService(this)
  lazy val jwtAuthenticator: JwtAuthenticator[RequestHeader, Response, JwtAuthenticationResult] = new PlayJwtAuthenticator(
    new PlayJwtRequestExtractor(),
    new PlayAuthenticationResultHandler(),
    new NimbusJwtReaderFactory(jwtIssuerService, jwtIssuerService))
}
