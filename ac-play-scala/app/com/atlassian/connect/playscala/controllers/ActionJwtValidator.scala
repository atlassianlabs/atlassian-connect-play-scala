package com.atlassian.connect.playscala.controllers

import com.atlassian.connect.playscala.auth.{ UserContext, UserInfo, Token }
import com.atlassian.connect.playscala.auth.jwt.JwtConfig
import com.atlassian.connect.playscala.model.ClientKey
import play.api.libs.json.{ Json, JsValue }
import play.api.mvc.{ Results, Result, RequestHeader }
import scala.concurrent.Future
import scalaz.\/
import scalaz.syntax.std.option._
import play.api.Logger

trait ActionJwtValidator extends JwtConfig {

  def jwtValidated(f: Token => Result)(implicit request: RequestHeader): Result =
    jwtValidatedGeneric(f) valueOr identity

  def jwtValidatedAsync(f: Token => Future[Result])(implicit request: RequestHeader): Future[Result] =
    jwtValidatedGeneric(f) valueOr Future.successful

  def jwtValidatedGeneric[A](validated: Token => A)(implicit request: RequestHeader): Result \/ A = {
    jwtAuthenticator.authenticate(request, null) flatMap {
      jwt =>
        Logger.debug(s"Successfully decoded JWT. iss is $jwt")
        val jsonPayload: JsValue = Json.parse(jwt.getJsonPayload)
        val userInfo =
          for {
            userKey <- (jsonPayload \ "context" \ "user" \ "userKey").asOpt[String]
            userName <- (jsonPayload \ "context" \ "user" \ "username").asOpt[String]
            userDisplayName <- (jsonPayload \ "context" \ "user" \ "displayName").asOpt[String]
          } yield UserInfo(userKey, userName, userDisplayName)
        (acHostModelStore.findByKey(ClientKey(jwt.getIssuer)) map { acHost =>
          val userContext =
            for {
              subject <- Option(jwt.getSubject)
            } yield UserContext(subject, userInfo)
          val token = Token(acHost, userContext)
          validated(token)
        }) \/> { Logger.debug(s"Issuer $jwt not found."); Results.BadRequest }
    }
  }
}
