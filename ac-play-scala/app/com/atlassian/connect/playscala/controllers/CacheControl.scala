package com.atlassian.connect.playscala.controllers

import play.api.Play
import play.api.mvc.{ Result, Request }
import scala.concurrent.{ ExecutionContext, Future }
import play.api.http.HeaderNames.CACHE_CONTROL

trait CacheControl {

  import play.api.Play.current

  lazy val DefaultCacheControl: Option[String] =
    current.configuration.getString("ac.cache-control").orElse(Some("no-cache").filter(_ => Play.isDev))

  protected final class CacheControl(cacheValue: String = null) extends ActionWrapper {

    private val calculatedCacheValue: Option[String] =
      Option(cacheValue) orElse DefaultCacheControl filterNot { _.trim.isEmpty }

    private def cacheHeader(result: Result): Option[String] = result.header.headers.get(CACHE_CONTROL)

    private def addCacheHeader(result: Result): Result = cacheHeader(result) orElse calculatedCacheValue match {
      case None => result
      case Some(value) => result.withHeaders(CACHE_CONTROL -> value)
    }

    def run[A](delegate: Request[A] => Future[Result])(implicit ec: ExecutionContext): Request[A] => Future[Result] =
      request => delegate(request) map addCacheHeader

  }
}
