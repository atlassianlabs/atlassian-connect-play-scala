package com.atlassian.connect.playscala.settings

import play.api.{ Application, GlobalSettings }
import play.api.mvc._
import play.filters.gzip.GzipFilter
import com.atlassian.connect.playscala.store.DefaultDbConfiguration

class AcGlobalSettings extends WithFilters(new GzipFilter()) with GlobalSettings {

  override def onStart(app: Application) {
    // Create DDL if needed
    DefaultDbConfiguration.onStart(app)
  }

}
