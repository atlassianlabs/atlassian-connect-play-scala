package com.atlassian.connect.playscala.store.slick

import com.atlassian.connect.playscala.model.AcHostModelStore
import com.atlassian.connect.playscala.store.DbConfiguration
import scala.slick.driver.JdbcProfile
import play.api.{ Logger, Application, Configuration }
import play.api.db.slick
import scala.slick.jdbc.meta.MTable

trait SlickDbConfiguration extends SlickDbSchema with DbConfiguration with SlickProfileLoader {

  import play.api.Play.current
  lazy val profile: JdbcProfile = loadProfile(current.configuration)

  lazy val acHostModelStore: AcHostModelStore = new AcHostModelSlickStore(this)

  override def onStart(app: Application): Unit = {
    import profile.SchemaDescription
    slick.DB(app) withSession { implicit session =>
      import profile.simple._
      if (MTable.getTables("ac_host").list.isEmpty) {
        Logger.info("Database table 'ac_host' not found. Creating...")
        val ddl: SchemaDescription = acHostModelSchema.ddl
        ddl.createStatements.foreach(statement => Logger.info(statement))
        ddl.create
        Logger.info("Database table 'ac_host' created.")
      }
    }
  }
}

trait SlickProfileLoader {

  private def slickDriverName(configuration: Configuration) = {
    configuration.getString("db.default.slick.driver").getOrElse(throw configuration.reportError("db.default.slick.driver", "No Slick Driver configured."))
  }

  def loadProfile(configuration: Configuration)(implicit manifest: Manifest[JdbcProfile]): JdbcProfile = {
    Class.forName(slickDriverName(configuration) + "$").getField("MODULE$").get(manifest.runtimeClass).asInstanceOf[JdbcProfile]
  }

}

object SlickDbConfiguration extends SlickDbConfiguration
