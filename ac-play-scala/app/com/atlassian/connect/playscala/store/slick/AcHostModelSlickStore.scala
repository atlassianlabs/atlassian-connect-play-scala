package com.atlassian.connect.playscala.store.slick

import com.atlassian.connect.playscala.model.{ AcHostModel, AcHostModelStore, ClientKey }
import play.api.Logger

class AcHostModelSlickStore(schema: SlickDbSchema) extends AcHostModelStore {

  import schema._
  import profile.simple._

  def withSlickSession[T](body: Session => T): T = {
    import play.api.Play.current
    play.api.db.slick.DB.withSession(body)
  }

  def all(): Seq[AcHostModel] = withSlickSession { implicit session =>
    Logger.debug("Finding all acHosts")
    acHostModelSchema.list
  }

  def findByKey(clientKey: ClientKey): Option[AcHostModel] = withSlickSession { implicit session =>
    Logger.debug(s"Finding by $clientKey")
    acHostModelSchema.filter(_.key === clientKey).firstOption
  }

  def findByUrl(baseUrl: String): Option[AcHostModel] = withSlickSession { implicit session =>
    acHostModelSchema.filter(_.baseUrl === baseUrl).firstOption
  }

  def save(acHost: AcHostModel) = withSlickSession { implicit session =>
    Logger.debug(s"Saving an AcHost $acHost")
    acHost.id.fold {
      val acHostModelAutoInc = acHostModelSchema returning acHostModelSchema.map(_.id)
      val newId = acHostModelAutoInc.insert(acHost)
      Logger.debug(s"Created a new AcHost $acHost with id $newId")
      acHost.copy(id = newId)
    } { existingId =>
      acHostModelSchema.filter(_.id === existingId).update(acHost)
      Logger.debug(s"Updated an existing AcHost $acHost")
      acHost
    }
  }
}
