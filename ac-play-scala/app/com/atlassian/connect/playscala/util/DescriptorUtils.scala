package com.atlassian.connect.playscala.util

import java.io.{ InputStream, File }
import com.atlassian.connect.playscala.AcConfig
import play.api.Logger

import scala.io.Source
import scala.util.control.NonFatal
import scalaz.\/

object DescriptorUtils {
  val ATLASSIAN_CONNECT_JSON_FILE_NAME = "atlassian-connect.json"

  def substituteVariables(sourceJson: String)(implicit acConfig: AcConfig): String = {
    // ouch very hacky. TODO: implement a proper solution for this
    implicit class StringReplaceOps(s: String) {
      def replaceIfNotEmpty(target: String, replacement: Option[String]): String =
        replacement.fold(s)(s.replace(target, _))
    }
    sourceJson
      .replace("${localBaseUrl}", acConfig.baseUrl)
      .replace("${addonName}", acConfig.pluginName)
      .replace("${addonKey}", acConfig.pluginKey)
      .replaceIfNotEmpty("${addonVersion}", acConfig.pluginVersion)
  }

  private def loadFromSource(source: => Source): Throwable \/ String =
    try {
      \/.right(source.mkString)
    } catch {
      case NonFatal(e) =>
        \/.left(e)
    } finally {
      try {
        source.close()
      } catch {
        case NonFatal(_) => ()
      }
    }

  private def fromFile(file: File): Throwable \/ Source = {
    Logger.debug(s"Trying to load Atlassian descriptor file from file system: $file")
    if (file.exists() && file.isFile && file.canRead) {
      Logger.debug(s"File $file exists, is a file and can be read.")
      \/.right(Source.fromFile(file))
    } else {
      val msg = s"File $file does not exist, is not a file, or cannot be read."
      Logger.debug(msg)
      \/.left(new Exception(msg))
    }
  }

  private def fromClassPath(resourceName: String): Throwable \/ Source = {
    Logger.debug(s"Trying to load Atlassian descriptor file from classpath: $resourceName")
    val inputStream: InputStream = getClass.getResourceAsStream(s"/$resourceName")
    if (inputStream != null) {
      Logger.debug(s"Found resource $resourceName in classpath")
      \/.right(Source.fromInputStream(inputStream))
    } else {
      val msg = s"Resource $resourceName cannot be loaded from classpath."
      Logger.debug(msg)
      \/.left(new Exception(msg))
    }
  }

  private def substituteVariablesFrom(source: Source)(implicit acConfig: AcConfig): Throwable \/ String =
    loadFromSource(source).map(text => substituteVariables(text)(acConfig))

  def substituteVariablesInFile(sourceJsonFile: File)(implicit acConfig: AcConfig): String =
    fromFile(sourceJsonFile) flatMap (s => substituteVariablesFrom(s)(acConfig)) valueOr (throw _)

  def substituteVariablesInFile(sourceJsonFile: String)(implicit acConfig: AcConfig): String =
    fromClassPath(sourceJsonFile) orElse
      fromFile(new File(sourceJsonFile)) flatMap { s =>
        substituteVariablesFrom(s)(acConfig)
      } getOrElse (throw new Exception(s"${sourceJsonFile} not found."))

  def substituteVariablesInDefaultFile(implicit acConfig: AcConfig): String =
    substituteVariablesInFile(ATLASSIAN_CONNECT_JSON_FILE_NAME)
}
