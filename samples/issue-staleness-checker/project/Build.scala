import sbt._
import com.typesafe.sbt.SbtScalariform.scalariformSettings
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName = "issue-staleness-checker"
  val appVersion = "0.1-SNAPSHOT"

  val playDep = Seq(
    "play" %% "play" % "2.1.1")

  val libDependencies = Seq(
    "org.scalaz" %% "scalaz-core" % "7.0.0-M8" withSources,
    "com.google.guava" % "guava" % "10.0.1" withSources,
    "commons-lang" % "commons-lang" % "2.4",
    "commons-fileupload" % "commons-fileupload" % "1.2.2" withSources,
    javaCore,
    javaEbean,
    "com.atlassian.connect" % "ac-play-scala_2.10" % "0.1-SNAPSHOT"
  )
//    "com.typesafe.slick" %% "slick" % "1.0.0"
//    "com.h2database" % "h2" % "1.3.170",
//    "postgresql" % "postgresql" % "8.4-702.jdbc4",
//    "com.google.api-client" % "google-api-client" % "1.12.0-beta",
//    "org.codehaus.janino" % "janino" % "2.6.1",
//    "ch.qos.logback" % "logback-core" % "1.0.9",
//    "ch.qos.logback" % "logback-classic" % "1.0.9",

  val sharedTestDependencies = Seq(
    "junit" % "junit" % "4.11" % "test",
    "org.specs2" %% "specs2" % "1.14" % "test",
    "org.mockito" % "mockito-all" % "1.9.5" % "test",
    "com.typesafe.akka" %% "akka-testkit" % "2.1.1" % "test" withSources ())

  val appDependencies = playDep ++ libDependencies

  val customResolvers = Seq(
    Resolver.defaultLocal,
    "atlassian-proxy" at "https://m2proxy.atlassian.com/content/groups/internal/",
    "atlassian-private-proxy" at "http://m2proxy-int.private.atlassian.com/content/groups/internal/",
    "atlassian-maven" at "http://maven.atlassian.com/content/groups/public/",
    "Local Maven Repository" at "file://" + Path.userHome + "/.m2/repository",
    Classpaths.typesafeReleases,
    DefaultMavenRepository)

  val main = play.Project(appName, appVersion, appDependencies ++ sharedTestDependencies)
    .settings(scalariformSettings: _*)
    .settings(playAssetsDirectories <+= baseDirectory / "aui")
    .settings(resolvers ++= customResolvers)

}
