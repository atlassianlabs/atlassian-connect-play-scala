// Comment to get more information during initialization
logLevel := Level.Warn

credentials := Seq(Credentials(Path.userHome / ".ivy2" / ".credentials"))

resolvers ++= Seq(
  "atlassian-proxy" at "https://m2proxy.atlassian.com/content/groups/internal/"
 ,"atlassian-private-proxy" at "http://m2proxy-int.private.atlassian.com/content/groups/internal/"
 ,"atlassian-maven" at "http://maven.atlassian.com/content/groups/public/"
 ,"sbt-idea-repo" at "http://mpeltonen.github.com/maven/"
 ,"typesafe-maven" at "http://repo.typesafe.com/typesafe/releases/"
 ,"scct-github-repository" at "http://mtkopone.github.com/scct/maven-repo"
 ,Resolver.url("artifactory", url("http://scalasbt.artifactoryonline.com/scalasbt/sbt-plugin-releases"))(Resolver.ivyStylePatterns)
)

// re-initialize full resolvers so the resolvers are used after local but before default external (see http://harrah.github.com/xsbt/latest/sxr/Defaults.scala.html#319732)
fullResolvers <<= (projectResolver, externalResolvers, sbtPlugin, sbtResolver) map { (pr,er,isPlugin,sr) =>
  val base = pr +: er
  if(isPlugin) sr +: base else base
}

// Use the Play sbt plugin for Play projects
addSbtPlugin("play" % "sbt-plugin" % "2.1.1")

addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse" % "2.1.1")

addSbtPlugin("reaktor" % "sbt-scct" % "0.2-SNAPSHOT")

addSbtPlugin("org.ensime" % "ensime-sbt-cmd" % "0.1.1")

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.2.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-scalariform" % "1.0.1")

scalacOptions ++= Seq("-deprecation")
